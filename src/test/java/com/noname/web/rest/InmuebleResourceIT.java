package com.noname.web.rest;

import com.noname.ReservasApp;
import com.noname.domain.Inmueble;
import com.noname.repository.InmuebleRepository;
import com.noname.service.InmuebleService;
import com.noname.service.dto.InmuebleDTO;
import com.noname.service.mapper.InmuebleMapper;
import com.noname.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.noname.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link InmuebleResource} REST controller.
 */
@SpringBootTest(classes = ReservasApp.class)
public class InmuebleResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRES = "AAAAAAAAAA";
    private static final String UPDATED_ADDRES = "BBBBBBBBBB";

    @Autowired
    private InmuebleRepository inmuebleRepository;

    @Autowired
    private InmuebleMapper inmuebleMapper;

    @Autowired
    private InmuebleService inmuebleService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restInmuebleMockMvc;

    private Inmueble inmueble;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final InmuebleResource inmuebleResource = new InmuebleResource(inmuebleService);
        this.restInmuebleMockMvc = MockMvcBuilders.standaloneSetup(inmuebleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Inmueble createEntity(EntityManager em) {
        Inmueble inmueble = new Inmueble()
            .name(DEFAULT_NAME)
            .addres(DEFAULT_ADDRES);
        return inmueble;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Inmueble createUpdatedEntity(EntityManager em) {
        Inmueble inmueble = new Inmueble()
            .name(UPDATED_NAME)
            .addres(UPDATED_ADDRES);
        return inmueble;
    }

    @BeforeEach
    public void initTest() {
        inmueble = createEntity(em);
    }

    @Test
    @Transactional
    public void createInmueble() throws Exception {
        int databaseSizeBeforeCreate = inmuebleRepository.findAll().size();

        // Create the Inmueble
        InmuebleDTO inmuebleDTO = inmuebleMapper.toDto(inmueble);
        restInmuebleMockMvc.perform(post("/api/inmuebles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inmuebleDTO)))
            .andExpect(status().isCreated());

        // Validate the Inmueble in the database
        List<Inmueble> inmuebleList = inmuebleRepository.findAll();
        assertThat(inmuebleList).hasSize(databaseSizeBeforeCreate + 1);
        Inmueble testInmueble = inmuebleList.get(inmuebleList.size() - 1);
        assertThat(testInmueble.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testInmueble.getAddres()).isEqualTo(DEFAULT_ADDRES);
    }

    @Test
    @Transactional
    public void createInmuebleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = inmuebleRepository.findAll().size();

        // Create the Inmueble with an existing ID
        inmueble.setId(1L);
        InmuebleDTO inmuebleDTO = inmuebleMapper.toDto(inmueble);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInmuebleMockMvc.perform(post("/api/inmuebles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inmuebleDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Inmueble in the database
        List<Inmueble> inmuebleList = inmuebleRepository.findAll();
        assertThat(inmuebleList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllInmuebles() throws Exception {
        // Initialize the database
        inmuebleRepository.saveAndFlush(inmueble);

        // Get all the inmuebleList
        restInmuebleMockMvc.perform(get("/api/inmuebles?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(inmueble.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].addres").value(hasItem(DEFAULT_ADDRES.toString())));
    }
    
    @Test
    @Transactional
    public void getInmueble() throws Exception {
        // Initialize the database
        inmuebleRepository.saveAndFlush(inmueble);

        // Get the inmueble
        restInmuebleMockMvc.perform(get("/api/inmuebles/{id}", inmueble.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(inmueble.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.addres").value(DEFAULT_ADDRES.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingInmueble() throws Exception {
        // Get the inmueble
        restInmuebleMockMvc.perform(get("/api/inmuebles/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInmueble() throws Exception {
        // Initialize the database
        inmuebleRepository.saveAndFlush(inmueble);

        int databaseSizeBeforeUpdate = inmuebleRepository.findAll().size();

        // Update the inmueble
        Inmueble updatedInmueble = inmuebleRepository.findById(inmueble.getId()).get();
        // Disconnect from session so that the updates on updatedInmueble are not directly saved in db
        em.detach(updatedInmueble);
        updatedInmueble
            .name(UPDATED_NAME)
            .addres(UPDATED_ADDRES);
        InmuebleDTO inmuebleDTO = inmuebleMapper.toDto(updatedInmueble);

        restInmuebleMockMvc.perform(put("/api/inmuebles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inmuebleDTO)))
            .andExpect(status().isOk());

        // Validate the Inmueble in the database
        List<Inmueble> inmuebleList = inmuebleRepository.findAll();
        assertThat(inmuebleList).hasSize(databaseSizeBeforeUpdate);
        Inmueble testInmueble = inmuebleList.get(inmuebleList.size() - 1);
        assertThat(testInmueble.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testInmueble.getAddres()).isEqualTo(UPDATED_ADDRES);
    }

    @Test
    @Transactional
    public void updateNonExistingInmueble() throws Exception {
        int databaseSizeBeforeUpdate = inmuebleRepository.findAll().size();

        // Create the Inmueble
        InmuebleDTO inmuebleDTO = inmuebleMapper.toDto(inmueble);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInmuebleMockMvc.perform(put("/api/inmuebles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inmuebleDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Inmueble in the database
        List<Inmueble> inmuebleList = inmuebleRepository.findAll();
        assertThat(inmuebleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteInmueble() throws Exception {
        // Initialize the database
        inmuebleRepository.saveAndFlush(inmueble);

        int databaseSizeBeforeDelete = inmuebleRepository.findAll().size();

        // Delete the inmueble
        restInmuebleMockMvc.perform(delete("/api/inmuebles/{id}", inmueble.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Inmueble> inmuebleList = inmuebleRepository.findAll();
        assertThat(inmuebleList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Inmueble.class);
        Inmueble inmueble1 = new Inmueble();
        inmueble1.setId(1L);
        Inmueble inmueble2 = new Inmueble();
        inmueble2.setId(inmueble1.getId());
        assertThat(inmueble1).isEqualTo(inmueble2);
        inmueble2.setId(2L);
        assertThat(inmueble1).isNotEqualTo(inmueble2);
        inmueble1.setId(null);
        assertThat(inmueble1).isNotEqualTo(inmueble2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(InmuebleDTO.class);
        InmuebleDTO inmuebleDTO1 = new InmuebleDTO();
        inmuebleDTO1.setId(1L);
        InmuebleDTO inmuebleDTO2 = new InmuebleDTO();
        assertThat(inmuebleDTO1).isNotEqualTo(inmuebleDTO2);
        inmuebleDTO2.setId(inmuebleDTO1.getId());
        assertThat(inmuebleDTO1).isEqualTo(inmuebleDTO2);
        inmuebleDTO2.setId(2L);
        assertThat(inmuebleDTO1).isNotEqualTo(inmuebleDTO2);
        inmuebleDTO1.setId(null);
        assertThat(inmuebleDTO1).isNotEqualTo(inmuebleDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(inmuebleMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(inmuebleMapper.fromId(null)).isNull();
    }
}
