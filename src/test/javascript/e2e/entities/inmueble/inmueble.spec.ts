/* tslint:disable no-unused-expression */
import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import InmuebleComponentsPage from './inmueble.page-object';
import { InmuebleDeleteDialog } from './inmueble.page-object';
import InmuebleUpdatePage from './inmueble-update.page-object';
import { waitUntilDisplayed, waitUntilHidden } from '../../util/utils';

const expect = chai.expect;

describe('Inmueble e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let inmuebleUpdatePage: InmuebleUpdatePage;
  let inmuebleComponentsPage: InmuebleComponentsPage;
  let inmuebleDeleteDialog: InmuebleDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
  });

  it('should load Inmuebles', async () => {
    await navBarPage.getEntityPage('inmueble');
    inmuebleComponentsPage = new InmuebleComponentsPage();
    expect(await inmuebleComponentsPage.getTitle().getText()).to.match(/Inmuebles/);
  });

  it('should load create Inmueble page', async () => {
    await inmuebleComponentsPage.clickOnCreateButton();
    inmuebleUpdatePage = new InmuebleUpdatePage();
    expect(await inmuebleUpdatePage.getPageTitle().getText()).to.match(/Create or edit a Inmueble/);
    await inmuebleUpdatePage.cancel();
  });

  it('should create and save Inmuebles', async () => {
    async function createInmueble() {
      await inmuebleComponentsPage.clickOnCreateButton();
      await inmuebleUpdatePage.setNameInput('name');
      expect(await inmuebleUpdatePage.getNameInput()).to.match(/name/);
      await inmuebleUpdatePage.setAddresInput('addres');
      expect(await inmuebleUpdatePage.getAddresInput()).to.match(/addres/);
      await waitUntilDisplayed(inmuebleUpdatePage.getSaveButton());
      await inmuebleUpdatePage.save();
      await waitUntilHidden(inmuebleUpdatePage.getSaveButton());
      expect(await inmuebleUpdatePage.getSaveButton().isPresent()).to.be.false;
    }

    await createInmueble();
    await inmuebleComponentsPage.waitUntilLoaded();
    const nbButtonsBeforeCreate = await inmuebleComponentsPage.countDeleteButtons();
    await createInmueble();

    await inmuebleComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeCreate + 1);
    expect(await inmuebleComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
  });

  it('should delete last Inmueble', async () => {
    await inmuebleComponentsPage.waitUntilLoaded();
    const nbButtonsBeforeDelete = await inmuebleComponentsPage.countDeleteButtons();
    await inmuebleComponentsPage.clickOnLastDeleteButton();

    const deleteModal = element(by.className('modal'));
    await waitUntilDisplayed(deleteModal);

    inmuebleDeleteDialog = new InmuebleDeleteDialog();
    expect(await inmuebleDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/reservasApp.inmueble.delete.question/);
    await inmuebleDeleteDialog.clickOnConfirmButton();

    await inmuebleComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeDelete - 1);
    expect(await inmuebleComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
