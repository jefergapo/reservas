import { element, by, ElementFinder } from 'protractor';

export default class InmuebleUpdatePage {
  pageTitle: ElementFinder = element(by.id('reservasApp.inmueble.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  nameInput: ElementFinder = element(by.css('input#inmueble-name'));
  addresInput: ElementFinder = element(by.css('input#inmueble-addres'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setNameInput(name) {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput() {
    return this.nameInput.getAttribute('value');
  }

  async setAddresInput(addres) {
    await this.addresInput.sendKeys(addres);
  }

  async getAddresInput() {
    return this.addresInput.getAttribute('value');
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
