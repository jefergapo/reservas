import { element, by, ElementFinder } from 'protractor';

export default class ReservaUpdatePage {
  pageTitle: ElementFinder = element(by.id('reservasApp.reserva.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  startDateInput: ElementFinder = element(by.css('input#reserva-startDate'));
  endDateInput: ElementFinder = element(by.css('input#reserva-endDate'));
  inmuebleSelect: ElementFinder = element(by.css('select#reserva-inmueble'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setStartDateInput(startDate) {
    await this.startDateInput.sendKeys(startDate);
  }

  async getStartDateInput() {
    return this.startDateInput.getAttribute('value');
  }

  async setEndDateInput(endDate) {
    await this.endDateInput.sendKeys(endDate);
  }

  async getEndDateInput() {
    return this.endDateInput.getAttribute('value');
  }

  async inmuebleSelectLastOption() {
    await this.inmuebleSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async inmuebleSelectOption(option) {
    await this.inmuebleSelect.sendKeys(option);
  }

  getInmuebleSelect() {
    return this.inmuebleSelect;
  }

  async getInmuebleSelectedOption() {
    return this.inmuebleSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
