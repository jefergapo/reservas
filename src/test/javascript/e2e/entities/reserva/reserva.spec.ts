/* tslint:disable no-unused-expression */
import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import ReservaComponentsPage from './reserva.page-object';
import { ReservaDeleteDialog } from './reserva.page-object';
import ReservaUpdatePage from './reserva-update.page-object';
import { waitUntilDisplayed, waitUntilHidden } from '../../util/utils';

const expect = chai.expect;

describe('Reserva e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let reservaUpdatePage: ReservaUpdatePage;
  let reservaComponentsPage: ReservaComponentsPage;
  let reservaDeleteDialog: ReservaDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
  });

  it('should load Reservas', async () => {
    await navBarPage.getEntityPage('reserva');
    reservaComponentsPage = new ReservaComponentsPage();
    expect(await reservaComponentsPage.getTitle().getText()).to.match(/Reservas/);
  });

  it('should load create Reserva page', async () => {
    await reservaComponentsPage.clickOnCreateButton();
    reservaUpdatePage = new ReservaUpdatePage();
    expect(await reservaUpdatePage.getPageTitle().getText()).to.match(/Create or edit a Reserva/);
    await reservaUpdatePage.cancel();
  });

  it('should create and save Reservas', async () => {
    async function createReserva() {
      await reservaComponentsPage.clickOnCreateButton();
      await reservaUpdatePage.setStartDateInput('01-01-2001');
      expect(await reservaUpdatePage.getStartDateInput()).to.eq('2001-01-01');
      await reservaUpdatePage.setEndDateInput('01-01-2001');
      expect(await reservaUpdatePage.getEndDateInput()).to.eq('2001-01-01');
      await reservaUpdatePage.inmuebleSelectLastOption();
      await waitUntilDisplayed(reservaUpdatePage.getSaveButton());
      await reservaUpdatePage.save();
      await waitUntilHidden(reservaUpdatePage.getSaveButton());
      expect(await reservaUpdatePage.getSaveButton().isPresent()).to.be.false;
    }

    await createReserva();
    await reservaComponentsPage.waitUntilLoaded();
    const nbButtonsBeforeCreate = await reservaComponentsPage.countDeleteButtons();
    await createReserva();

    await reservaComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeCreate + 1);
    expect(await reservaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
  });

  it('should delete last Reserva', async () => {
    await reservaComponentsPage.waitUntilLoaded();
    const nbButtonsBeforeDelete = await reservaComponentsPage.countDeleteButtons();
    await reservaComponentsPage.clickOnLastDeleteButton();

    const deleteModal = element(by.className('modal'));
    await waitUntilDisplayed(deleteModal);

    reservaDeleteDialog = new ReservaDeleteDialog();
    expect(await reservaDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/reservasApp.reserva.delete.question/);
    await reservaDeleteDialog.clickOnConfirmButton();

    await reservaComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeDelete - 1);
    expect(await reservaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
