import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IInmueble, defaultValue } from 'app/shared/model/inmueble.model';

export const ACTION_TYPES = {
  FETCH_INMUEBLE_LIST: 'inmueble/FETCH_INMUEBLE_LIST',
  FETCH_INMUEBLE: 'inmueble/FETCH_INMUEBLE',
  CREATE_INMUEBLE: 'inmueble/CREATE_INMUEBLE',
  UPDATE_INMUEBLE: 'inmueble/UPDATE_INMUEBLE',
  DELETE_INMUEBLE: 'inmueble/DELETE_INMUEBLE',
  RESET: 'inmueble/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IInmueble>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type InmuebleState = Readonly<typeof initialState>;

// Reducer

export default (state: InmuebleState = initialState, action): InmuebleState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_INMUEBLE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_INMUEBLE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_INMUEBLE):
    case REQUEST(ACTION_TYPES.UPDATE_INMUEBLE):
    case REQUEST(ACTION_TYPES.DELETE_INMUEBLE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_INMUEBLE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_INMUEBLE):
    case FAILURE(ACTION_TYPES.CREATE_INMUEBLE):
    case FAILURE(ACTION_TYPES.UPDATE_INMUEBLE):
    case FAILURE(ACTION_TYPES.DELETE_INMUEBLE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_INMUEBLE_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_INMUEBLE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_INMUEBLE):
    case SUCCESS(ACTION_TYPES.UPDATE_INMUEBLE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_INMUEBLE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/inmuebles';

// Actions

export const getEntities: ICrudGetAllAction<IInmueble> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_INMUEBLE_LIST,
  payload: axios.get<IInmueble>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IInmueble> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_INMUEBLE,
    payload: axios.get<IInmueble>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IInmueble> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_INMUEBLE,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IInmueble> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_INMUEBLE,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IInmueble> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_INMUEBLE,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
