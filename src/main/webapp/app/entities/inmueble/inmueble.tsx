import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './inmueble.reducer';
import { IInmueble } from 'app/shared/model/inmueble.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IInmuebleProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class Inmueble extends React.Component<IInmuebleProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { inmuebleList, match } = this.props;
    return (
      <div>
        <h2 id="inmueble-heading">
          Inmuebles
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp; Create new Inmueble
          </Link>
        </h2>
        <div className="table-responsive">
          {inmuebleList && inmuebleList.length > 0 ? (
            <Table responsive>
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Addres</th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {inmuebleList.map((inmueble, i) => (
                  <tr key={`entity-${i}`}>
                    <td>
                      <Button tag={Link} to={`${match.url}/${inmueble.id}`} color="link" size="sm">
                        {inmueble.id}
                      </Button>
                    </td>
                    <td>{inmueble.name}</td>
                    <td>{inmueble.addres}</td>
                    <td className="text-right">
                      <div className="btn-group flex-btn-group-container">
                        <Button tag={Link} to={`${match.url}/${inmueble.id}`} color="info" size="sm">
                          <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${inmueble.id}/edit`} color="primary" size="sm">
                          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${inmueble.id}/delete`} color="danger" size="sm">
                          <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                        </Button>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            <div className="alert alert-warning">No Inmuebles found</div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ inmueble }: IRootState) => ({
  inmuebleList: inmueble.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Inmueble);
