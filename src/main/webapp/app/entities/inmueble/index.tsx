import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Inmueble from './inmueble';
import InmuebleDetail from './inmueble-detail';
import InmuebleUpdate from './inmueble-update';
import InmuebleDeleteDialog from './inmueble-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={InmuebleUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={InmuebleUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={InmuebleDetail} />
      <ErrorBoundaryRoute path={match.url} component={Inmueble} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={InmuebleDeleteDialog} />
  </>
);

export default Routes;
