import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IReserva, defaultValue } from 'app/shared/model/reserva.model';

export const ACTION_TYPES = {
  FETCH_RESERVA_LIST: 'reserva/FETCH_RESERVA_LIST',
  FETCH_RESERVA: 'reserva/FETCH_RESERVA',
  CREATE_RESERVA: 'reserva/CREATE_RESERVA',
  UPDATE_RESERVA: 'reserva/UPDATE_RESERVA',
  DELETE_RESERVA: 'reserva/DELETE_RESERVA',
  RESET: 'reserva/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IReserva>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type ReservaState = Readonly<typeof initialState>;

// Reducer

export default (state: ReservaState = initialState, action): ReservaState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_RESERVA_LIST):
    case REQUEST(ACTION_TYPES.FETCH_RESERVA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_RESERVA):
    case REQUEST(ACTION_TYPES.UPDATE_RESERVA):
    case REQUEST(ACTION_TYPES.DELETE_RESERVA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_RESERVA_LIST):
    case FAILURE(ACTION_TYPES.FETCH_RESERVA):
    case FAILURE(ACTION_TYPES.CREATE_RESERVA):
    case FAILURE(ACTION_TYPES.UPDATE_RESERVA):
    case FAILURE(ACTION_TYPES.DELETE_RESERVA):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_RESERVA_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_RESERVA):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_RESERVA):
    case SUCCESS(ACTION_TYPES.UPDATE_RESERVA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_RESERVA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/reservas';

// Actions

export const getEntities: ICrudGetAllAction<IReserva> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_RESERVA_LIST,
  payload: axios.get<IReserva>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IReserva> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_RESERVA,
    payload: axios.get<IReserva>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IReserva> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_RESERVA,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IReserva> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_RESERVA,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IReserva> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_RESERVA,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
