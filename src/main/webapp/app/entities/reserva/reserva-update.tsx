import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IInmueble } from 'app/shared/model/inmueble.model';
import { getEntities as getInmuebles } from 'app/entities/inmueble/inmueble.reducer';
import { getEntity, updateEntity, createEntity, reset } from './reserva.reducer';
import { IReserva } from 'app/shared/model/reserva.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IReservaUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IReservaUpdateState {
  isNew: boolean;
  inmuebleId: string;
}

export class ReservaUpdate extends React.Component<IReservaUpdateProps, IReservaUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      inmuebleId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getInmuebles();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { reservaEntity } = this.props;
      const entity = {
        ...reservaEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/reserva');
  };

  render() {
    const { reservaEntity, inmuebles, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="reservasApp.reserva.home.createOrEditLabel">Create or edit a Reserva</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : reservaEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="reserva-id">ID</Label>
                    <AvInput id="reserva-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="startDateLabel" for="reserva-startDate">
                    Start Date
                  </Label>
                  <AvField id="reserva-startDate" type="date" className="form-control" name="startDate" />
                </AvGroup>
                <AvGroup>
                  <Label id="endDateLabel" for="reserva-endDate">
                    End Date
                  </Label>
                  <AvField id="reserva-endDate" type="date" className="form-control" name="endDate" />
                </AvGroup>
                <AvGroup>
                  <Label for="reserva-inmueble">Inmueble</Label>
                  <AvInput id="reserva-inmueble" type="select" className="form-control" name="inmuebleId">
                    <option value="" key="0" />
                    {inmuebles
                      ? inmuebles.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.name}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/reserva" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  inmuebles: storeState.inmueble.entities,
  reservaEntity: storeState.reserva.entity,
  loading: storeState.reserva.loading,
  updating: storeState.reserva.updating,
  updateSuccess: storeState.reserva.updateSuccess
});

const mapDispatchToProps = {
  getInmuebles,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReservaUpdate);
