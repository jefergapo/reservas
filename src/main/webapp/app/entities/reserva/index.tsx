import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Reserva from './reserva';
import ReservaDetail from './reserva-detail';
import ReservaUpdate from './reserva-update';
import ReservaDeleteDialog from './reserva-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ReservaUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ReservaUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ReservaDetail} />
      <ErrorBoundaryRoute path={match.url} component={Reserva} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ReservaDeleteDialog} />
  </>
);

export default Routes;
