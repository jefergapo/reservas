import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './reserva.reducer';
import { IReserva } from 'app/shared/model/reserva.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IReservaProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class Reserva extends React.Component<IReservaProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { reservaList, match } = this.props;
    return (
      <div>
        <h2 id="reserva-heading">
          Reservas
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp; Create new Reserva
          </Link>
        </h2>
        <div className="table-responsive">
          {reservaList && reservaList.length > 0 ? (
            <Table responsive>
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Start Date</th>
                  <th>End Date</th>
                  <th>Inmueble</th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {reservaList.map((reserva, i) => (
                  <tr key={`entity-${i}`}>
                    <td>
                      <Button tag={Link} to={`${match.url}/${reserva.id}`} color="link" size="sm">
                        {reserva.id}
                      </Button>
                    </td>
                    <td>
                      <TextFormat type="date" value={reserva.startDate} format={APP_LOCAL_DATE_FORMAT} />
                    </td>
                    <td>
                      <TextFormat type="date" value={reserva.endDate} format={APP_LOCAL_DATE_FORMAT} />
                    </td>
                    <td>{reserva.inmuebleName ? <Link to={`inmueble/${reserva.inmuebleId}`}>{reserva.inmuebleName}</Link> : ''}</td>
                    <td className="text-right">
                      <div className="btn-group flex-btn-group-container">
                        <Button tag={Link} to={`${match.url}/${reserva.id}`} color="info" size="sm">
                          <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${reserva.id}/edit`} color="primary" size="sm">
                          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${reserva.id}/delete`} color="danger" size="sm">
                          <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                        </Button>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            <div className="alert alert-warning">No Reservas found</div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ reserva }: IRootState) => ({
  reservaList: reserva.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Reserva);
