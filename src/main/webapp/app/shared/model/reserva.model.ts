import { Moment } from 'moment';

export interface IReserva {
  id?: number;
  startDate?: Moment;
  endDate?: Moment;
  inmuebleName?: string;
  inmuebleId?: number;
}

export const defaultValue: Readonly<IReserva> = {};
