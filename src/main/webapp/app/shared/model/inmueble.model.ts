export interface IInmueble {
  id?: number;
  name?: string;
  addres?: string;
}

export const defaultValue: Readonly<IInmueble> = {};
