package com.noname.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.noname.domain.Inmueble} entity.
 */
public class InmuebleDTO implements Serializable {

    private Long id;

    private String name;

    private String addres;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddres() {
        return addres;
    }

    public void setAddres(String addres) {
        this.addres = addres;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InmuebleDTO inmuebleDTO = (InmuebleDTO) o;
        if (inmuebleDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), inmuebleDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "InmuebleDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", addres='" + getAddres() + "'" +
            "}";
    }
}
