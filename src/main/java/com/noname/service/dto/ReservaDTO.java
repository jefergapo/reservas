package com.noname.service.dto;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.noname.domain.Reserva} entity.
 */
public class ReservaDTO implements Serializable {

    private Long id;

    private LocalDate startDate;

    private LocalDate endDate;


    private Long inmuebleId;

    private String inmuebleName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Long getInmuebleId() {
        return inmuebleId;
    }

    public void setInmuebleId(Long inmuebleId) {
        this.inmuebleId = inmuebleId;
    }

    public String getInmuebleName() {
        return inmuebleName;
    }

    public void setInmuebleName(String inmuebleName) {
        this.inmuebleName = inmuebleName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReservaDTO reservaDTO = (ReservaDTO) o;
        if (reservaDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reservaDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ReservaDTO{" +
            "id=" + getId() +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", inmueble=" + getInmuebleId() +
            ", inmueble='" + getInmuebleName() + "'" +
            "}";
    }
}
