package com.noname.service;

import com.noname.service.dto.InmuebleDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.noname.domain.Inmueble}.
 */
public interface InmuebleService {

    /**
     * Save a inmueble.
     *
     * @param inmuebleDTO the entity to save.
     * @return the persisted entity.
     */
    InmuebleDTO save(InmuebleDTO inmuebleDTO);

    /**
     * Get all the inmuebles.
     *
     * @return the list of entities.
     */
    List<InmuebleDTO> findAll();


    /**
     * Get the "id" inmueble.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<InmuebleDTO> findOne(Long id);

    /**
     * Delete the "id" inmueble.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
