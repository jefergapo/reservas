package com.noname.service.impl;

import com.noname.service.InmuebleService;
import com.noname.domain.Inmueble;
import com.noname.repository.InmuebleRepository;
import com.noname.service.dto.InmuebleDTO;
import com.noname.service.mapper.InmuebleMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Inmueble}.
 */
@Service
@Transactional
public class InmuebleServiceImpl implements InmuebleService {

    private final Logger log = LoggerFactory.getLogger(InmuebleServiceImpl.class);

    private final InmuebleRepository inmuebleRepository;

    private final InmuebleMapper inmuebleMapper;

    public InmuebleServiceImpl(InmuebleRepository inmuebleRepository, InmuebleMapper inmuebleMapper) {
        this.inmuebleRepository = inmuebleRepository;
        this.inmuebleMapper = inmuebleMapper;
    }

    /**
     * Save a inmueble.
     *
     * @param inmuebleDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public InmuebleDTO save(InmuebleDTO inmuebleDTO) {
        log.debug("Request to save Inmueble : {}", inmuebleDTO);
        Inmueble inmueble = inmuebleMapper.toEntity(inmuebleDTO);
        inmueble = inmuebleRepository.save(inmueble);
        return inmuebleMapper.toDto(inmueble);
    }

    /**
     * Get all the inmuebles.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<InmuebleDTO> findAll() {
        log.debug("Request to get all Inmuebles");
        return inmuebleRepository.findAll().stream()
            .map(inmuebleMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one inmueble by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<InmuebleDTO> findOne(Long id) {
        log.debug("Request to get Inmueble : {}", id);
        return inmuebleRepository.findById(id)
            .map(inmuebleMapper::toDto);
    }

    /**
     * Delete the inmueble by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Inmueble : {}", id);
        inmuebleRepository.deleteById(id);
    }
}
