package com.noname.service.mapper;

import com.noname.domain.*;
import com.noname.service.dto.InmuebleDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Inmueble} and its DTO {@link InmuebleDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface InmuebleMapper extends EntityMapper<InmuebleDTO, Inmueble> {



    default Inmueble fromId(Long id) {
        if (id == null) {
            return null;
        }
        Inmueble inmueble = new Inmueble();
        inmueble.setId(id);
        return inmueble;
    }
}
