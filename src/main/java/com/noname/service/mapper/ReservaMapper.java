package com.noname.service.mapper;

import com.noname.domain.*;
import com.noname.service.dto.ReservaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Reserva} and its DTO {@link ReservaDTO}.
 */
@Mapper(componentModel = "spring", uses = {InmuebleMapper.class})
public interface ReservaMapper extends EntityMapper<ReservaDTO, Reserva> {

    @Mapping(source = "inmueble.id", target = "inmuebleId")
    @Mapping(source = "inmueble.name", target = "inmuebleName")
    ReservaDTO toDto(Reserva reserva);

    @Mapping(source = "inmuebleId", target = "inmueble")
    Reserva toEntity(ReservaDTO reservaDTO);

    default Reserva fromId(Long id) {
        if (id == null) {
            return null;
        }
        Reserva reserva = new Reserva();
        reserva.setId(id);
        return reserva;
    }
}
