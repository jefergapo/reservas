/**
 * View Models used by Spring MVC REST controllers.
 */
package com.noname.web.rest.vm;
