package com.noname.web.rest;

import com.noname.service.InmuebleService;
import com.noname.web.rest.errors.BadRequestAlertException;
import com.noname.service.dto.InmuebleDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.noname.domain.Inmueble}.
 */
@RestController
@RequestMapping("/api")
public class InmuebleResource {

    private final Logger log = LoggerFactory.getLogger(InmuebleResource.class);

    private static final String ENTITY_NAME = "inmueble";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final InmuebleService inmuebleService;

    public InmuebleResource(InmuebleService inmuebleService) {
        this.inmuebleService = inmuebleService;
    }

    /**
     * {@code POST  /inmuebles} : Create a new inmueble.
     *
     * @param inmuebleDTO the inmuebleDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new inmuebleDTO, or with status {@code 400 (Bad Request)} if the inmueble has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/inmuebles")
    public ResponseEntity<InmuebleDTO> createInmueble(@RequestBody InmuebleDTO inmuebleDTO) throws URISyntaxException {
        log.debug("REST request to save Inmueble : {}", inmuebleDTO);
        if (inmuebleDTO.getId() != null) {
            throw new BadRequestAlertException("A new inmueble cannot already have an ID", ENTITY_NAME, "idexists");
        }
        InmuebleDTO result = inmuebleService.save(inmuebleDTO);
        return ResponseEntity.created(new URI("/api/inmuebles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /inmuebles} : Updates an existing inmueble.
     *
     * @param inmuebleDTO the inmuebleDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated inmuebleDTO,
     * or with status {@code 400 (Bad Request)} if the inmuebleDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the inmuebleDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/inmuebles")
    public ResponseEntity<InmuebleDTO> updateInmueble(@RequestBody InmuebleDTO inmuebleDTO) throws URISyntaxException {
        log.debug("REST request to update Inmueble : {}", inmuebleDTO);
        if (inmuebleDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        InmuebleDTO result = inmuebleService.save(inmuebleDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, inmuebleDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /inmuebles} : get all the inmuebles.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of inmuebles in body.
     */
    @GetMapping("/inmuebles")
    public List<InmuebleDTO> getAllInmuebles() {
        log.debug("REST request to get all Inmuebles");
        return inmuebleService.findAll();
    }

    /**
     * {@code GET  /inmuebles/:id} : get the "id" inmueble.
     *
     * @param id the id of the inmuebleDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the inmuebleDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/inmuebles/{id}")
    public ResponseEntity<InmuebleDTO> getInmueble(@PathVariable Long id) {
        log.debug("REST request to get Inmueble : {}", id);
        Optional<InmuebleDTO> inmuebleDTO = inmuebleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(inmuebleDTO);
    }

    /**
     * {@code DELETE  /inmuebles/:id} : delete the "id" inmueble.
     *
     * @param id the id of the inmuebleDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/inmuebles/{id}")
    public ResponseEntity<Void> deleteInmueble(@PathVariable Long id) {
        log.debug("REST request to delete Inmueble : {}", id);
        inmuebleService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
