package com.noname.repository;

import com.noname.domain.Inmueble;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Inmueble entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InmuebleRepository extends JpaRepository<Inmueble, Long> {

}
